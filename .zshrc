# Don't do anything, if not running interactively
[[ $- != *i* ]] && return

# MPD daemon start (if no other user instance exists)
# [ ! -s ~/.config/mpd/pid ] && mpd
# Use this instead: systemctl --user enable mpd 

# Enable colors
autoload -U colors && colors

# Allow commands in prompts
setopt prompt_subst
# username@host [git branch] as prompt
PROMPT='%F{green}%n%f@%F{magenta}%m%f %F{yellow}$(git rev-parse --abbrev-ref HEAD 2>/dev/null) %F{blue}%B%~%b%f %# '

# Use history file
if [[ ! -a ~/.cache/zsh/zsh_history ]]; then
    mkdir -p ~/.cache/zsh   
    touch ~/.cache/zsh/zsh_history
fi
HISTFILE=~/.cache/zsh/zsh_history
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory

# Better tab completion
autoload -U compinit
compinit

# No longer need cd to switch dirs
setopt autocd

# Add aliases
if [[ -a ~/.zsh_aliases ]]; then
    source ~/.zsh_aliases
fi

# etc file auto backup
# lines in .etcs should start with /etc/
ETCFILES_FILE=~/.etcs
if [ ! -f $ETCFILES_FILE ]; then
    touch $ETCFILES_FILE
fi

stringToReplace="/etc"
stringReplacement="$HOME/.etc"
for etcfile in `cat $ETCFILES_FILE`; do
    convertedPath=$(echo $etcfile | sed "s-$stringToReplace-$stringReplacement-")
    if [ ! -d "$etc" ]; then 
        dirToMake=${convertedPath%/*}
        sudo mkdir -p "$dirToMake"
        sudo cp -r $etcfile $convertedPath
    else
        sudo cp -r $etcfile $convertedPath   
    fi
done

# Dotfiles tracker
# Add tracked dotfiles to Git automatically
DOTFILES_FILE=~/.dotfiles
git add -f $DOTFILES_FILE
if [ ! -f $DOTFILES_FILE ]; then
    touch $DOTFILES_FILE
fi

for dotfile in `cat $DOTFILES_FILE`; do
    git add -f "$HOME/$dotfile"
done

git commit -m "Update dotfiles"
git push
