# Maintain packages file
pacman -Qqe > .installed-packages

# Maintain fstab backup
cp /etc/fstab ~/.fstab

# Maintain /boot backup
cp -r /boot ~/.boot

# Run SSH agent
if ! pgrep -u "$USER" s sh-agent > /dev/null; then
    ssh-agent > "$XDG_RUNTIME_DIR/ssh-agent.env"
fi
if [[ ! "$SSH_AUTH_SOCK" ]]; then
    eval "$(<"$XDG_RUNTIME_DIR/ssh-agent.env")"
fi
